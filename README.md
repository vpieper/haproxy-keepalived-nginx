HAProxy - NGINX
===============

Lint
----

- Install the ansible-lint python module

```bash
python -m venv env
source env/bin/activate

pip install ansible-lint
```

- Check the vesion of ansible-lint

```bash
ansible-lint --version
```

- Check code

```bash
ansible-lint site.yml
```

---

Only messages about not using latest packages should be shown.

```bash
[403] Package installs should not use latest
roles/haproxy-keepalived/tasks/haproxy.yml:5
Task/Handler: Ensure dependencies are installed

[403] Package installs should not use latest
roles/haproxy-keepalived/tasks/haproxy.yml:18
Task/Handler: Ensure haproxy

[403] Package installs should not use latest
roles/haproxy-keepalived/tasks/keepalived.yml:1
Task/Handler: Ensure keepalived

[403] Package installs should not use latest
roles/nginx/tasks/nginx.yml:1
Task/Handler: Ensure nginx
```

---
