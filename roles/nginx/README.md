nginx
=========

Installation of nginx.

Requirements
------------

None.

Role Variables
--------------

None.

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: webservers
  gather_facts: false
  become: yes
  roles:
    - nginx
```

License
-------

MIT

Author Information
------------------

Jeroen van de Lockand
